class AddAttachmentImageToWeixins < ActiveRecord::Migration
  def self.up
    change_table :weixins do |t|
      t.attachment :image
    end
  end

  def self.down
    remove_attachment :weixins, :image
  end
end
