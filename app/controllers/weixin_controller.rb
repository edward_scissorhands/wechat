class WeixinController < ApplicationController
  skip_before_filter :verify_authenticity_token
  before_filter :get_token, only: [:create]
  def index
    @gallery = Weixin.all
  end

  def create
    # if params[:xml][:MsgType] == "text"
    #   render "text", layout: false, format: :xml
    # end
    get_token
    case params[:xml][:MsgType]
      when "event"
        if params[:xml][:Event] == "subscribe"
          render "subscribe", layout: false, format: :xml
        end
      when "text"
        render "text", layout: false, formats: :xml
      when "image"
        @image_remote_url = Weixin.new
        image_url = params[:xml][:PicUrl]
        @image_remote_url.avatar_remote_url= image_url
        @image_remote_url.save
        render "echo", layout: false, formats: :xml
      else
    end
  end

  def validate
    render text: params[:echostr]
  end

  private
  def validate_wechat
    array = [Rails.configuration.weixin_token, params[:timestamp], params[:nonce]].sort
    if params[:signature] != Digest::SHA1.hexdigest(array.join)
      render text: "Forbidden", :status => 403
    end
  end

  def get_token
    require 'net/http'
    url = URI('https://api.weixin.qq.com/cgi-bin/token')
    params = { grant_type: "client_credential", appid: "wx90ad430687fadcae", secret: "ab64cf9a1f562f6df22c845a071f50af" }
    url.query = URI.encode_www_form(params)

    res = Net::HTTP.get_response(url)
    puts res.body if res.is_a?(Net::HTTPSuccess)
  end
end
