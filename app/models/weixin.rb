class Weixin < ActiveRecord::Base
  attr_reader :image_remote_url
  has_attached_file :image
  validates_attachment_content_type :image, :content_type => ["image/jpg", "image/jpeg", "image/png", "image/gif"]

  def avatar_remote_url=(url_value)
    self.image = URI.parse(url_value)
    @image_remote_url = url_value
  end
end
