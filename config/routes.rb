Rails.application.routes.draw do
  # devise_for :users
  root 'static_pages#home'

  get 'weixin', to: 'weixin#validate'
  get 'gallery', to: 'weixin#index'
  resources :weixin, only: [:create]
end
